set nobackup
set undofile           " keep an undo file (undo changes after closing)
set ruler              " show the cursor position all the time
set showcmd            " display incomplete commands
set title              " show filename in window title
set expandtab          " spaces for tab
set shiftwidth=4
set tabstop=4
set smarttab
set shellslash
set nohlsearch
" allow switching buffers with changes
set hidden

set nowrapscan
set wildignore=*.o,*~,*.pyc

set autoread

"set guifont=DejaVu Sans Mono:h10:cANSI
let s:fontsize = 12
function! ResetFontSize()
    let s:fontsize = 12
    :execute "GuiFont! Consolas:h" . s:fontsize
endfunction

function! AdjustFontSize(amount)
    let s:fontsize = s:fontsize+a:amount
    :execute "GuiFont! Consolas:h" . s:fontsize
endfunction

noremap <C-ScrollWheelUp> :call AdjustFontSize(1)<CR>
noremap <C-ScrollWheelDown> :call AdjustFontSize(-1)<CR>
inoremap <C-ScrollWheelUp> :call AdjustFontSize(1)<CR>a
inoremap <C-ScrollWheelDown> :call AdjustFontSize(-1)<CR>a

noremap <C-=> :call AdjustFontSize(1)<CR>
noremap <C--> :call AdjustFontSize(-1)<CR>
noremap <C-0> :call ResetFontSize()<CR>

let g:loaded_netrw = 1
let g:loaded_newrwPlugin = 1

colorscheme desert

" Don't use Ex mode, use Q for formatting
noremap Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" Switch syntax highlighting on
syntax on

" Also switch on highlighting the last used search pattern.
" set hlsearch

" I like highlighting strings inside C comments.
let c_comment_strings=1

" Enable file type detection.
" Use the default filetype settings, so that mail gets 'textwidth' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
filetype plugin indent on

" Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
  autocmd!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") |
    \   execute "normal! g`\"" |
    \ endif

augroup END

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_ | diffthis
                 \ | wincmd p | diffthis
endif

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Paste on shift-insert
" https://superuser.com/a/322953
map <silent> <S-Insert> "+p
imap <silent> <S-Insert> <Esc>"+p

" Python syntax highlighting for Vim
" https://github.com/vim-python/python-syntax
let g:python_highlight_all = 1

" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('$LOCALAPPDATA/nvim/plugged')

" Make sure you use single quotes

Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'andrewradev/sideways.vim'
Plug 'bling/vim-airline'
Plug 'chr4/nginx.vim'
Plug 'christoomey/vim-sort-motion'
Plug 'dhruvasagar/vim-table-mode'
Plug 'ervandew/supertab'
Plug 'jdonaldson/vaxe'
Plug 'mmikeww/autohotkey.vim'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'vim-python/python-syntax'
Plug 'tpope/vim-fugitive'

" Initialize plugin system
call plug#end()
