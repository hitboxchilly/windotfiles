export HISTCONTROL=ignoreboth
export PAGER=less

alias dotfiles="$(which git)"' --git-dir=$HOME/.dotfiles --work-tree=$HOME'
alias flask='winpty flask'
alias grep='grep --color=always'
alias ll='ls -lh --group-directories-first'
alias ls='ls --color --group-directories-first'
alias pgcli='winpty  pgcli'
alias py='winpty -Xallow-non-tty py'
alias python='winpty -Xallow-non-tty python'
# PostreSQL
alias psql='winpty -Xallow-non-tty psql'
alias createdb='winpty -Xallow-non-tty createdb'
alias dropdb='winpty -Xallow-non-tty dropdb'

activate() {
    paths=( "venv/Scripts/activate" "env/Scripts/activate" )
    for path in "${paths[@]}"; do
        if [ -f "$path" ]; then
            source "$path"
            break
        fi
    done
}

reactivate() {
    deactivate
    activate
}

findpy() {
    # find python files in given dir, excluding virtual environment directory
    find $1 -name "*.py" -not -path "./venv/*"
}

tree_func() {
    cmd //C tree //F $1
}
alias tree=tree_func

eval "$(dircolors -b /etc/DIR_COLORS)"
