def init_pyreadline():
    import sys
    import os
    try:
        import interactive_prompt_toolkit
    except ImportError:
        print('Module interactive_prompt_toolkit not available.', file=sys.stderr)
    else:
        os.environ['LESS'] = ' -R'
        os.environ['LESSOPEN'] = '| pygmentize -l python %s'
        interactive_prompt_toolkit.install(interactive_prompt_toolkit.readline)

init_pyreadline()
del init_pyreadline
