set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=$HOME/vimfiles/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
call vundle#begin('$HOME/vimfiles/bundle/')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'AndrewRadev/sideways.vim'
Plugin 'Glench/Vim-Jinja2-Syntax'
Plugin 'ap/vim-css-color'
Plugin 'bling/vim-airline'
Plugin 'christoomey/vim-sort-motion'
Plugin 'ervandew/supertab'
Plugin 'godlygeek/tabular'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'scrooloose/nerdtree'
Plugin 'sjl/badwolf'
Plugin 'tmhedberg/SimpylFold'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-surround'
Plugin 'wellle/targets.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

let g:airline#extensions#tabline#enabled = 1 " smarter tab line

" vim-colors-solarized
syntax enable
colorscheme desert

" FINALLY! THIS disables that damn popup
set completeopt-=preview

set guifont=DejaVu_Sans_Mono:h10:cANSI
set guioptions-=T
set guioptions-=m
set guioptions-=r
set guioptions-=L

set autoindent
set expandtab
set shiftwidth=4
"set smartindent
set smarttab
set tabstop=4

set incsearch
set nowrapscan

set laststatus=2    " always show status line

set nobackup
set nowritebackup
set noswapfile

set wildmenu
set wildignore=*.o,*~,*.pyc

" allow switching buffers with changes
set hidden

" Use dash a word separator
"set iskeyword-=-
"set iskeyword-=_

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" windows
au GUIEnter * simalt ~x " maximize on windows

set title

set viewdir=~/vimfiles/views

set foldmethod=indent
set foldlevel=99

:command! WQ wq
:command! Wq wq
:command! W w
:command! Q q

set shellslash
set wildmode=longest,list,full
